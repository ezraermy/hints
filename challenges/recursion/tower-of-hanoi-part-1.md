# Recursion: Towers of Hanoi - Part 1

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

In this challenge, we will focus on the most basic sub-problem of the Tower of Hanoi problem.


First, let's recall the rules for moving the disks:<br>
&emsp;\- Only one disk may be moved at a time.<br>
&emsp;\- The pegs must be in ascending order.<br>

The challenge here is not just to move all the disks to the goal peg, but to complete it in the least number of steps.

If there is only one disk, we can solve the problem in just one step by moving the disk onto the goal peg.

<img src="images/recursion/tower-of-hanoi-part-1/hanoi-part-1-1.png"  width="800"><br>

However, if there are two disks, we need a couple of moves to accomplish this task.

<b>`Hint:`&nbsp;The largest disk at the bottom of the starting peg can be moved only if there is no disk above it.<br>
<b>`Hint:`&nbsp;The largest disk can be moved onto the goal peg only if the goal peg is empty.<br>
<b>`Hint:`&nbsp;The intermediate peg must be used. <br>

<b>`Note:`</b>&nbsp;We can calculate the intermediate peg from the given parameters.<br>

</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;Order of the required moves</summary><br>

To find the intermediate peg from the given paremeters, we can simply use:

```ruby
def move(starting, goal)
  intermediate = ([1, 2, 3] - [starting, goal]).first
end
```

<img src="images/recursion/tower-of-hanoi-part-1/hanoi-part-1-2.png"  width="800">

To get to the bottom disk of the starting peg while keeping the goal peg empty, we need to:</b><br>
<b>`Hint:`&nbsp;Move the top disk onto the intermediate peg.</b><br>
<b>`Hint:`&nbsp;Then, move the bottom disk onto the goal peg.</b><br>
<b>`Hint:`&nbsp;Finally, move the small disk (originally at the top of the starting peg) onto the goal peg from the intermediate peg.</b><br>
  
</details>
<!-- Hint 2 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution</summary><br>

```ruby
def move(starting, goal)
  intermediate = ([1, 2, 3] - [starting, goal]).first
  # Step 1: move the top disk onto the intermediate peg
  result = move_text(starting, intermediate) + " "
  # Step 2: move the largest disk onto the goal peg
  result += move_text(starting, goal) + " "
  # Step 3: move the intermediate disk onto the goal peg
  result+= move_text(intermediate, goal)
  result
end

# helper function that returns the move in a string format
def move_text(from, to)
"#{from}->#{to}"
end
```
  
</details>
<!-- Solution -->




